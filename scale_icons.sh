#!/bin/bash

rm -v 64x64/apps/$1
convert 128x128/apps/$1 -verbose -resize 64x 64x64/apps/$1
rm -v 48x48/apps/$1
convert 128x128/apps/$1 -verbose -resize 48x 48x48/apps/$1
rm -v 48x48/apps/$1
convert 128x128/apps/$1 -verbose -resize 32x 32x32/apps/$1
rm -v 22x22/apps/$1
convert 128x128/apps/$1 -verbose -resize 22x 22x22/apps/$1
rm -v 16x16/apps/$1
convert 128x128/apps/$1 -verbose -resize 16x 16x16/apps/$1
